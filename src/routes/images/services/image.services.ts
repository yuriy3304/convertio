import * as fsPromises from "fs/promises";
import * as fs from "fs";
import * as path from "path";
import QueueService from "../../../utils/queue";

const pathRoot = path.resolve(__dirname, '../../../../');

type ImageData = {
  userId: string,
  id: string,
}

export default class ImageService {
  static async getAllImages (data: ImageData) {
    QueueService.add();
    if (data.userId) {
      const filesFolder = `${pathRoot}/static/${data.userId}`;
      if (fs.existsSync(path.dirname(filesFolder))) {
        return await fsPromises.readdir(filesFolder);
      }
    }
    return null;
  }
  static async getImage (data: ImageData) {
    if (data.userId) {
      const filesFolder = `${pathRoot}/static/${data.userId}`;
      if (fs.existsSync(path.dirname(filesFolder))) {
        if (data.id) {
          const filePath = `${filesFolder}/${data.id}`;
          if (fs.existsSync(filePath)) {
            return filePath;
          }
        }
      }
    }
    return null;
  }
}
