import { Request, Response } from "express";
import ImageService from "../services/image.services";

export class ImageController {
  constructor() {}
  async getAllImages(req: Request, res: Response) {
    const data = {
      id: 'im5.png',
      userId: '111',
    }
    const images = await ImageService.getAllImages(data);
    res.json({
      user: req,
      images
    });
  }

  async getImage(req: Request, res: Response) {
    const id = req.params.id;
    res.json({id});
  }

  async upload(req: Request, res: Response) {
    const { files } = req;
    try {
      
    } catch (e) {
      
    }
  }
}

export default new ImageController();
