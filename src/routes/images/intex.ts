import * as express from 'express';
import ImageController from "./controllers/image.controller";
// import { storage } from "../../configs/multer.config";
import multer from "multer";

// const uploadMiddleware = multer({ storage }).array('files');
const routes = express.Router();

routes.get('/', ImageController.getAllImages);
routes.get('/:id', ImageController.getImage);
// routes.post('/upload', uploadMiddleware, ImageController.upload);
routes.post('/upload', ImageController.upload);

export { routes };
