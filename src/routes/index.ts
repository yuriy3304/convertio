import * as express from 'express';

import { routes as imagesRoutes } from './images/intex';
import {Request, Response} from "express";
import {getKeycloak} from "../configs/keycloak-config.config";

const routes = express.Router();


routes.use('/images', imagesRoutes);
routes.use('/', (req: Request, res: Response) => {

  console.log(req);
  res.json(req?.user ?? { user: 'not found' });
});

export { routes };
