import session from "express-session";

declare module 'express-session' {
   interface SessionData {
      views?: number;
      userId?: string;
      "keycloak-token"?: string;
      data: Record<string, { views: number }>;
     }
}
