export interface PassportUser {
  id?: number | string;
  username: string;
  password: string;
  source?: string;
}
