import express from 'express';
import bodyParser from "body-parser";
import morgan from "morgan";
import { routes } from "./routes";
import { serverPort } from "./configs/env.dev";
import session from "express-session";
// import { initKeycloak } from "./configs/keycloak-config.config";
// import Keycloak from 'keycloak-connect';

const app = express();
const port = serverPort;
// const keycloak = initKeycloak();
const memoryStore = new session.MemoryStore();
app.set( 'trust proxy', true );
app.use(session({
  secret: 'keyboard cat',
  resave: false,
  saveUninitialized: true,
  store: memoryStore
}));
// const keycloak = new Keycloak({
//   store: memoryStore,
// })
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
// app.use(keycloak.middleware())
// app.get('/test', (req, res) => {
//   console.log(keycloak);
//   res.json({
//     keycloak,
//     res: req.session
//   });
// })
//keycloak.protect(),
// app.get('/login', keycloak.protect('realm:admin'), function (req, res) {
//   res.json({
//     result: req.session,
//     event: '1. Authentication\n2. Login'
//   })
// })
app.use(bodyParser.json());
app.use(morgan('dev'));
// app.use(routes);
app.get('/', (req, res) => {
  res.json({success: 'ok7'});
})

app.listen(port, () => {
  console.log(`App port - ${port}`);
})
