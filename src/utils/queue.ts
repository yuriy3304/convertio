class QueueService {
  users: Map<string, User> = new Map<string, User>;
  limits: number = 0;

  constructor() {}

  add() {

  }
  addUser(user: User) {
    this.users.set(user.id, user);
  }
}

class File {
  constructor() {
  }
}

class User {
  id: string;
  files: File[] = [];
  queue: QueueService;
  constructor(id: string, queue: QueueService) {
    this.id = id;
    this.queue = queue;
  }
}

// export const queueService = ActiveQueue.getInstance();
export default new QueueService();
