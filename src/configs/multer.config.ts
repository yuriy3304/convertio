import path from "path";
import multer from 'multer';
import { nanoid } from "nanoid";
import { filePath } from "./env.dev";
import { Request } from "express";

// const storage = multer.diskStorage({
//   destination: function (req: Request, file, cb) {
//     console.log(file)
//     const { originalname = '' } = file;
//     const endFolder = req?.user ?? 'free';
//     cb(null, `${path.join(__dirname, '..', '..', filePath, endFolder)}`);
//   },
//   filename: function (req: Request, file, cb) {
//     const fileId = nanoid();
//     cb(null, `${fileId}-${Date.now()}${path.extname(file.originalname)}`);
//   }
// })
//
// export { storage };
