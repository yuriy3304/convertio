const session = require('express-session');
const Keycloak = require('keycloak-connect');

let _keycloak: any;

const keycloakConfig = {
  clientId: 'nodejs',
  bearerOnly: true,
  serverUrl: 'http://localhost:8080/auth',
  realm: 'Demo-Realm',
  credentials: {
    secret: 'QHm1QTjON3OMZcEDLASsnBVShmY99t46'
  }
};

function initKeycloak(): any {
  if (_keycloak) {
    console.warn("Trying to init Keycloak again!");
    return _keycloak;
  }
  else {
    console.log("Initializing Keycloak...");
    var memoryStore = new session.MemoryStore();
    _keycloak = new Keycloak({ store: memoryStore }, keycloakConfig);
    return _keycloak;
  }
}

function getKeycloak(): any {
  if (!_keycloak){
    console.error('Keycloak has not been initialized. Please called init first.');
  }
  return _keycloak;
}

export {
  initKeycloak,
  getKeycloak
}
