const dotenv = require("dotenv");

dotenv.config();

const filePath = process.env.FILE_PATH ?? '';
const clientOriginUrl = process.env.CLIENT_ORIGIN_URL ?? '';
const serverPort = process.env.SERVER_PORT ?? '';
const googleClient = process.env.GOOGLE_CLIENT_ID ?? '';
const googleClientSecret = process.env.GOOGLE_CLIENT_SECRET ?? '';
const googleCallback = process.env.GOOGLE_CALLBACK ?? '';

const clientOrigins = ["http://localhost:4040"];

export {
  googleClient,
  googleCallback,
  googleClientSecret,
  serverPort,
  clientOriginUrl,
  clientOrigins,
  filePath
};
